
resource "aws_instance" "backend" {
  ami           = var.instance_ami
  instance_type = var.instance_type

  subnet_id = aws_subnet.subnet_public.id
  vpc_security_group_ids = [ aws_security_group.sg_22.id, aws_security_group.sg_ping.id ]
  key_name = aws_key_pair.ec2key.key_name
  tags = {
    Environment = var.environment_tag
    Name = "Java Runtime Version 8 Backend"
  }
}

resource "aws_instance" "frontend" {
  ami           = var.instance_ami
  instance_type = var.instance_type

  subnet_id = aws_subnet.subnet_public.id
  vpc_security_group_ids = [ aws_security_group.sg_22.id, aws_security_group.sg_80.id, aws_security_group.sg_ping.id ]
  key_name = aws_key_pair.ec2key.key_name
  tags = {
    Environment = var.environment_tag
    Name = "Nginx Frontend"
  }
}

resource "aws_elasticache_cluster" "memcached" {
  cluster_id           = "cluster-memcached"
  engine               = "memcached"
  engine_version       = "1.4.34"
  node_type            = "cache.t3.micro"
  num_cache_nodes      = 4
  availability_zone    = var.availability_zone
  parameter_group_name = "default.memcached1.4"
  port                 = 11211
  subnet_group_name    = aws_elasticache_subnet_group.memcached.name
}


output "host_groups" {
  value = [
    # {
    #   group = "memcached",
    #   hosts = [for i in aws_instance.memcached: {
    #       public_ip = i.public_ip,
    #       private_ip = i.private_ip,
    #       id = i.id,
    #       extra_vars = {}
    #     }
    #   ],
    #   extra_vars = {}
    # },
    {
      group = "memcached",
      hosts = [
        {
          id = aws_elasticache_cluster.memcached.id,
          public_ip = aws_elasticache_cluster.memcached.cluster_address, 
          private_ip = aws_elasticache_cluster.memcached.configuration_endpoint,
          extra_vars = {}
        }
      ],
      extra_vars = {}
    },
    {
      group = "backend",
      hosts = [
        {
          public_ip = aws_instance.backend.public_ip,
          private_ip = aws_instance.backend.private_ip,
          id = aws_instance.backend.id,
          extra_vars = {}
        }
      ],
      extra_vars = {}
    },
    {
      group = "frontend",
      hosts = [
        {
          public_ip = aws_instance.frontend.public_ip,
          private_ip = aws_instance.frontend.private_ip,
          id = aws_instance.frontend.id,
          extra_vars = {}
        }
      ],
      extra_vars = {}
    },
    {
      group = "servers",
      hosts = [
        {
          public_ip = aws_instance.backend.public_ip,
          private_ip = aws_instance.backend.private_ip,
          id = aws_instance.backend.id,
          extra_vars = {}
        },
        {
          public_ip = aws_instance.frontend.public_ip,
          private_ip = aws_instance.frontend.private_ip,
          id = aws_instance.frontend.id,
          extra_vars = {}
        }
      ],
      extra_vars = {}
    }
  ]
}

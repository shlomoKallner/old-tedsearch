variable "cidr_vpc" {
  description = "CIDR block for the VPC"
  default = "10.1.0.0/16"
}
variable "cidr_subnet" {
  description = "CIDR block for the subnet"
  default = "10.1.1.0/24"
}
variable "cidr_memcached_subnet" {
  description = "CIDR block for the subnet"
  default = "10.1.2.0/24"
}
variable "availability_zone" {
  description = "availability zone to create subnet"
  default = "us-east-2a"
}
variable "public_key_path" {
  description = "Public key path"
  default = "~/.ssh/id_aws_terraform.pem.pub"
}
variable "instance_ami" {
  description = "AMI for aws EC2 instance"
  default = "ami-0d5d9d301c853a04a"
}
variable "instance_type" {
  description = "type for aws EC2 instance"
  default = "t2.micro"
}
variable "environment_tag" {
  description = "Environment tag"
  default = "Production"
}